FROM alpine:3.13.1 AS base

# gcc is apparantly incapable of building a static binary, 
# even gcc -static helloworld.c ends up linked to libc, 
# instead of solving, use clang
ENV CC=clang

RUN apk add --no-cache gnupg \
        build-base \
        curl \
        clang \
        openssl-dev \
        nghttp2-dev \
        nghttp2-static \
        libssh2-dev \
        libssh2-static \
        openssl-libs-static \
        zlib-static \
        groff \
        python3-dev \
        libpsl-dev \
        stunnel \
        perl && \
    curl https://curl.haxx.se/ca/cacert.pem -L -o /cacert.pem

FROM base as builder

USER nobody
RUN set -x && \
    tmp_dir="/tmp/build" && \
    mkdir ${tmp_dir} && \
    chown nobody:nobody $tmp_dir && \
    export HOME=${tmp_dir} && \
    cd ${tmp_dir} && \
    # add Daniel Stenberg key
    gpg --keyserver keys.gnupg.net --recv-keys 27EDEAF22F3ABCEB50DB9A125CC908FDB71E12C2 && \
    for i in "tar.gz" "tar.gz.asc"; \
    do \
        curl -sLo code.${i} `curl -s https://api.github.com/repos/curl/curl/releases/latest \
            | grep "browser_download_url" \
            | cut -d '"' -f 4 \
            | grep "${i}$"` ;\
    done; \
    gpg --verify code.tar.gz.asc code.tar.gz &&\
    mkdir curl &&\
    tar xzf code.tar.gz -C curl --strip-components 1 &&\
    cd curl &&\
    CFLAGS="-march=x86-64 -mtune=generic -O2" \
    LDFLAGS="-static" \
    PKG_CONFIG="pkg-config --static" \
    ./configure --disable-shared --enable-static --disable-ldap --enable-ipv6 --enable-unix-sockets --with-ssl --with-libssh2 &&\
    make -j$(nproc) V=1 curl_LDFLAGS=-all-static &&\
    #make test && \
    strip src/curl

USER root

RUN mv /tmp/build/curl/src/curl /curl

FROM scratch

COPY --from=builder /curl /curl
COPY --from=builder "/cacert.pem" "/etc/ssl/certs/ca-certificates.crt"

ENTRYPOINT ["/curl"]
